﻿// See https://aka.ms/new-console-template for more information
using otus_multi_thread;
using System.Diagnostics;

Console.WriteLine("Hello, World!");

//сумма массива интов 100 000, 1 000 000, 10 000 000
Generator _gen = new();
Summator _sum = new();
int[] arr100k = _gen.GetArray(100000);
int[] arr1kk = _gen.GetArray(1000000);
int[] arr10kk = _gen.GetArray(10000000);
//последовательное вычисление
Console.WriteLine("Dump sum");
Console.WriteLine($"   100 000 = {_sum.DumpSum(arr100k)}");
Console.WriteLine($" 1 000 000 = {_sum.DumpSum(arr1kk)}");
Console.WriteLine($"10 000 000 = {_sum.DumpSum(arr10kk)}");
//List and Thread
Console.WriteLine("Thread sum");
Console.WriteLine($"   100 000 = {_sum.ListSum(arr100k, 4)}");
Console.WriteLine($" 1 000 000 = {_sum.ListSum(arr1kk, 4)}");
Console.WriteLine($"10 000 000 = {_sum.ListSum(arr10kk, 4)}");
//PLINQ
Console.WriteLine("PLINQ sum");
Console.WriteLine($"   100 000 = {_sum.LINQSum(arr100k)}");
Console.WriteLine($" 1 000 000 = {_sum.LINQSum(arr1kk)}");
Console.WriteLine($"10 000 000 = {_sum.LINQSum(arr10kk)}");

Console.ReadKey();