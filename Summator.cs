﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace otus_multi_thread
{
    public class Summator
    {
        private ConcurrentBag<int> _tempSum = new ConcurrentBag<int>();
        public Summator()
        {
        }
        /// <summary>
        /// простая сумма циклом for
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public TimeSpan DumpSum(int[] arr)
        {
            Stopwatch sw = Stopwatch.StartNew();
            double dump_sum = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                dump_sum += arr[i];
            }            
            sw.Stop();
            Console.WriteLine($"sum = {dump_sum}");
            return sw.Elapsed;
        }
        /// <summary>
        /// сумма с помощью List и Thread
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="threadcount"></param>
        /// <returns></returns>
        public TimeSpan ListSum(int[] arr, int threadcount)
        {
            _tempSum.Clear();
            Stopwatch sw = Stopwatch.StartNew();
            List<Thread> list = new List<Thread>();
            for (int i = 0; i < threadcount; i++)
            {
                int tmp = i;
                Thread tempThread = new Thread(() => OneThreadCalc(tmp, arr, threadcount));
                list.Add(tempThread);
                list[i].Start();
            }
            //ожидаем завершения потоков
            foreach (Thread tr in list) 
            {
                tr.Join();
            }
            int _finalSum = 0;
            foreach (int i in _tempSum)
            {
                _finalSum += i;
            }            
            sw.Stop();
            Console.WriteLine($"sum Thread = {_finalSum}");
            return sw.Elapsed;
        }
        /// <summary>
        /// сумма с помощью LINQ
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public TimeSpan LINQSum(int[] arr)
        {
            Stopwatch sw = Stopwatch.StartNew();            
            int _sum = arr.AsParallel().Sum(x => x);

            /*
            int _sum = 0;
            _ = Parallel.For<int>(0, arr.Length, () => 0, (i, state, subtotal) =>
            {
                subtotal += arr[i];
                return subtotal;
            },
            (subtotal) => Interlocked.Add(ref _sum, subtotal));*/
            sw.Stop();
            Console.WriteLine($"sum PLINQ = {_sum}");
            return sw.Elapsed;
        }
        private void OneThreadCalc(int threadid, int[] array, int numberofthreads)
        {
            int thid = threadid;
            int _sum = 0;
            for (int i = thid * (array.Length / numberofthreads); i < (thid + 1) * array.Length / numberofthreads; i++)
            {
                _sum += array[i];                
            }
            _tempSum.Add(_sum);
        }
    }
}
